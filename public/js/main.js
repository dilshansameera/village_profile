$(document).ready(function(){
	$('#province').on('change', function(e){
		getList('district', $(this).val(), $('#district'));
	});
	$('#district').on('change', function(e){
		getList('agadiv', $(this).val(), $('#agadiv'));
		getList('postoffice', $(this).val(), $('#postoffice'));
	});
	/*$('#agadiv').on('change', function(e){
		getList('gndiv', $(this).val(), $('#gndiv'));
	});
*/
	function getList(type, id, target){
		var form_data = {
			type: type,
			id: id,
		};
		$.ajax({
			url: "/getlist",
			type: 'GET',
			data: form_data,
			success: function(data) {
				console.log(data);
				genList(target, data);
			},
			error:function (xhr, ajaxOptions, thrownError){
				alert(xhr.status);
			}
		});
	}
	function genList(target, data){
		target.empty().append($("<option></option>").attr("value",null).text('Please select'));
		$.each(data, function(key, value) {
		target.append($("<option></option>")
			.attr("value",key)
			.text(value));
		});
	}
});