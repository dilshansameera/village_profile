<?php

namespace App\Models;

use Moloquent;

class Postoffice extends Moloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'Postoffice';
    protected $primaryKey = "_id";
    protected $fillable = [
    	'name'
    ];

    public function District()
    {
        return $this->belongsTo('App\Models\District', 'district_id');
    }

   

}
