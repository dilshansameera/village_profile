<?php

namespace App\Models;

use Moloquent;

class Postal extends Moloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'postal';
    protected $primaryKey = "_id";
    protected $fillable = [
    	'name','postal_code', 'district_id', 'si'
    ];

    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id');
    }
    
    public function villages()
    {
        return $this->hasMany('App\Models\Village', 'postoffice');
    }

   

}
