<?php

namespace App\Models;

use Moloquent;

class District extends Moloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'district';
    protected $primaryKey = "_id";
    protected $fillable = [
    	'name','province_id', 'si'
    ];

    public function province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id');
    }

    public function agadivs()
    {
        return $this->hasMany('App\Models\Agadiv', 'district_id');
    }
    public function postoffices()
    {
        return $this->hasMany('App\Models\Postal', 'district_id');
    }

    public function villages()
    {
        return $this->hasMany('App\Models\Village', 'district_id');
    }

}
