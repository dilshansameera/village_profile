<?php

namespace App\Models;

use Moloquent;

class Gndiv extends Moloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'Gndiv';
    protected $primaryKey = "_id";
    protected $fillable = [
    	'name','district_id','agadiv_id'
    ];

    public function Agadiv()
    {
        return $this->belongsTo('App\Models\Agadiv', 'agadiv_id');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id');
    }

   

}
