<?php

namespace App\Models;

use Moloquent;

class Location extends Moloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'location';
    protected $primaryKey = "_id";
    protected $fillable = [
    	'name','description','village_id', 'latlng'
    ];

    public function village()
    {
        return $this->belongsTo('App\Models\Village', 'village_id');
    }
    public function images()
    {
        return $this->embedsMany('App\Models\Locationimage');
    }

   

}
