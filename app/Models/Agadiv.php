<?php

namespace App\Models;

use Moloquent;

class Agadiv extends Moloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'agadiv';
    protected $primaryKey = "_id";
    protected $fillable = [
    	'name','district_id', 'si'
    ];

    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id');
    }
    public function gndivs()
    {
        return $this->hasMany('App\Models\Gndiv', 'agadiv_id');
    }
    public function villages()
    {
        return $this->hasMany('App\Models\Village', 'agadiv_id');
    }

   

}
