<?php

namespace App\Models;

use Moloquent;

class Person extends Moloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'person';
    protected $primaryKey = "_id";
    protected $fillable = [
    	'name','description','village_id'
    ];

    public function village()
    {
        return $this->belongsTo('App\Models\Village', 'village_id');
    }
    public function images()
    {
        return $this->embedsMany('App\Models\Personimage');
    }

   

}
