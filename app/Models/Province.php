<?php

namespace App\Models;

use Moloquent;


class Province extends Moloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'province';
    protected $primaryKey = "_id";
    protected $fillable = [
    	'name'
    ];

    public function districts()
    {
        return $this->hasMany('App\Models\District', 'province_id');
    }
    public function villages()
    {
        return $this->hasMany('App\Models\Village', 'province_id');
    }
}
