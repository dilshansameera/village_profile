<?php

namespace App\Models;

use Moloquent;

class Village extends Moloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'village';
    protected $primaryKey = "_id";
    protected $fillable = [
    	'name', 'village_num','province_id', 'district_id', 'agadiv_id','othernames','description', 'postoffice', 'nearest_town',
        'climate','geo_variations','geo_type', 'forest_type','forest_desc', 'plants',
        'societies','groups','organisations','business', 'latlng','translate'
    ];

    public function images()
    {
        return $this->embedsMany('App\Models\Villageimage');
    }
    public function locations()
    {
        return $this->hasMany('App\Models\Location', 'village_id');
    }
    public function people()
    {
        return $this->hasMany('App\Models\Person', 'village_id');
    }
   
    public function province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id');
    }
    public function agadiv()
    {
        return $this->belongsTo('App\Models\Agadiv', 'agadiv_id');
    }
    public function postal()
    {
        return $this->belongsTo('App\Models\Postal', 'postoffice');
    }
}
