<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Person;
use App;
//use Request;
use Input;
use Image;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $people = Person::all();  
        return view('dashboard/person/index', ['people'=>$people]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $village_id = $request->input('village_id'); 
        $data = ['village_id'=>$village_id];
        return view('dashboard/person/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();
        $loc = Person::create($input);
        if(Input::file()){
            $this->upload($loc->_id, $request);
        }
        
        return redirect('village/'.$input['village_id']);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $person = Person::findOrFail($id);  

        //return $village;
        return view('dashboard/person/show',['person'=>$person, 'images'=>$person->images()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $person = Person::findOrFail($id);
        return view('dashboard/person/edit',['person'=>$person]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $person = Person::findOrFail($id); 
        $person->update($request->all()) ;
       return redirect('person/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $person = Person::findOrFail($id);
        $img = $person->images()->all();
        foreach ($img as $i) {
            unlink(public_path('img/person/'.$i->name));
        }
        $person->delete(); 
        //$village = App\Models\Village::find($location->village_id);
        return redirect('village/'.$person->village_id);
    }

    public function destroyImge($pid, $id)
    {
        $person = Person::findOrFail($pid);
        $img = $person->images()->find($id);
        unlink(public_path('img/person/'.$img->name));
        $img->delete();
        //$village->images()->delete($id);
        //unlink(public_path('img/village/'.$img->name));
        return redirect('person/'.$pid);
    }

    public function upload($id, Request $request){

        $image = Input::file('file');
        $filename  = time() . '.' . $image->getClientOriginalExtension();
        $filename  = time() . '.' . $id.'.jpg';
        $path = public_path('img/person/'.$filename);
        Image::make($image->getRealPath())->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path);
        $person = Person::findOrFail($id);
        $image = $person->images()->create(array('name' => $filename));
        return redirect('person/'.$id);
        
    }


}
