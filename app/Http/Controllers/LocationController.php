<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Location;
use App;
//use Request;
use Input;
use Image;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::all();  
        return view('dashboard/location/index', ['locations'=>$locations]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $village_id = $request->input('village_id'); 
        $village = App\Models\Village::findOrFail($village_id);
        $data = ['village_id'=>$village_id, 'village'=>$village];
        return view('dashboard/location/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();
        $loc = Location::create($input);
        if(Input::file()){
            $this->upload($loc->_id, $request);
        }
        
        return redirect('village/'.$input['village_id']);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $location = Location::findOrFail($id);  

        //return $village;
        return view('dashboard/location/show',['location'=>$location, 'images'=>$location->images()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $location = Location::findOrFail($id);
        return view('dashboard/location/edit',['location'=>$location]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $location = Location::findOrFail($id); 
        $location->update($request->all()) ;
       return redirect('location/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::findOrFail($id);
        $img = $location->images()->all();
        foreach ($img as $i) {
            unlink(public_path('img/location/'.$i->name));
        }
        $location->delete(); 
        //$village = App\Models\Village::find($location->village_id);
        return redirect('village/'.$location->village_id);
    }
    public function destroyImge($lid, $id)
    {
        $location = Location::findOrFail($lid);
        $img = $location->images()->find($id);
        unlink(public_path('img/location/'.$img->name));
        $img->delete();
        //$village->images()->delete($id);
        //unlink(public_path('img/village/'.$img->name));
        return redirect('location/'.$lid);
    }
    public function upload($id, Request $request){

        $image = Input::file('file');
        $filename  = time() . '.' . $image->getClientOriginalExtension();
        $filename  = time() . '.' . $id.'.jpg';
        $path = public_path('img/location/'.$filename);
        Image::make($image->getRealPath())->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path);
        $location = Location::findOrFail($id);
        $image = $location->images()->create(array('name' => $filename));
        return redirect('location/'.$id);
        
    }


}
