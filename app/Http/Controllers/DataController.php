<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Village;
use App\Models\Province;
use App\Models\District;
use App\Models\Agadiv;
use App\Models\Postal;
use App;
//use Request;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    public function provinces(){
        $provinces=['Central','Eastern','North Central','Northern','North Western','Sabaragamuwa','Southern','Uva','Western'];
        foreach ($provinces as $val) {
            $data = ['name'=>$val];
            //Province::create($data);
        }
        return Province::all();
       
    }

    public function districts()
    {
        $d = District::all();
        foreach ($d as $key) {
            echo $key->name.'-'.$key->_id.'<br>';
        }

        /*$csv = dirname(__FILE__).DIRECTORY_SEPARATOR.'sl_districts.csv';    
        $row = 1;
        if (($handle = fopen($csv, "r")) !== FALSE) {
            while (($rowdata = fgetcsv($handle, 1000, ",")) !== FALSE) {         
                $row++;
                    $data = array(                            
                            'name' =>$rowdata[1],
                            'province_id' =>$rowdata[2],
                            'si' =>['name'=>$rowdata[0]]
                            );
                    //District::create($data);
                    print_r($data);
                
            }
            fclose($handle);
        }*/

    }
    public function agas()
    {
        $d = District::all();
        foreach ($d as $key) {
            
            $csv = dirname(__FILE__).DIRECTORY_SEPARATOR.'agas'.DIRECTORY_SEPARATOR.$key->name.'.csv'; 

            if (($handle = fopen($csv, "r")) !== FALSE) {
                while (($rowdata = fgetcsv($handle, 1000, ",")) !== FALSE) {                
                        $data = array(                            
                                'name' =>ucfirst(strtolower(trim($rowdata[1]))),
                                'district_id' =>$key->_id,
                                'si' =>['name'=>trim($rowdata[0])]
                                );
                        Agadiv::create($data);
                        //echo $data['name'].'-'.$data['district_id'].'-'.$data['si']['name'].'<br>';                
                }
                fclose($handle);
            }
        }          

    }
    public function postals()
    {
        $d = District::all();
        foreach ($d as $key) {
            
            $csv = dirname(__FILE__).DIRECTORY_SEPARATOR.'postal'.DIRECTORY_SEPARATOR.$key->name.'.csv'; 
            if (file_exists($csv)) {               
                if (($handle = fopen($csv, "r")) !== FALSE) {
                    while (($rowdata = fgetcsv($handle, 1000, ",")) !== FALSE) {                
                            $data = array(                            
                                    'name' =>ucfirst(strtolower(trim($rowdata[0]))),
                                    'district_id' =>$key->_id,
                                    'postal_code' => trim($rowdata[1])
                                    );
                            Postal::create($data);
                            echo $data['name'].'-'.$data['district_id'].'-'.$key->name.'<br>';                
                    }
                    fclose($handle);
                }
             
            }
        }          

    }

    
}
