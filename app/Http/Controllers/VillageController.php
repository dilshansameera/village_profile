<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Village;
use App\Models\Province;
use App\Models\Agadiv;
use App\Models\District;
use App;
//use Request;
use Input;
use Image;
use Excel;

class VillageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        //$provinces = Province::pluck('name', '_id'); 
        $provinces = Province::all()->pluck('name', '_id'); 
        //return $provinces; 
        
        $villages = '';
        if(Input::has('agadiv_id') && Input::get('agadiv_id') != 'Please select'){
            $agadiv = Agadiv::findOrFail(Input::get('agadiv_id')); 
            $villages = $agadiv->villages;
        }else if(Input::has('district_id') && Input::get('district_id') != 'Please select'){
            $district = District::findOrFail(Input::get('district_id')); 
            $villages = $district->villages;  
            
        }else if (Input::has('province_id') && Input::get('province_id') != 'Please select')
        {
            $Province = Province::findOrFail(Input::get('province_id'));  
            $villages = $Province->villages;  
        } else{
            $villages = Village::all();  
        }

        return view('dashboard/village/index', ['villages'=>$villages,'provinces'=>$provinces]);
        // return view('master',['villages'=>$villages]);
        
    }

    public function getlist(Request $request){
        $type = $request->input('type');
        $id = $request->input('id');
        switch ($type) {
            case 'district':
                $list = Province::find($id)->districts->lists('name', '_id'); 
                break;
            case 'agadiv':
                $list = District::find($id)->agadivs->lists('name', '_id'); 
                break;
            case 'gndiv':
                $list = Agadiv::find($id)->gndivs->lists('name', '_id'); 
                break;
             case 'postoffice':
                $list = District::find($id)->postoffices->lists('name', '_id'); 
                break;
            default:
                # code...
                break;
        }        
        return $list;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::all()->pluck('name', '_id'); 
        $data = ['provinces'=>$provinces];
        return view('dashboard/village/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();
        Village::create($input);
        return redirect('village');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $village = Village::findOrFail($id);  
        $locations = $village->locations;
        $people = $village->people;
        //print_r($locations);
        //dd($village);
        return view('dashboard/village/show',['village'=>$village, 'images'=>$village->images(), 'locations'=>$locations, 'people'=>$people]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $village = Village::findOrFail($id); 
        $provinces = Province::lists('name', '_id');
        $districts = Province::find($village->province_id)->districts->lists('name', '_id'); 
        $agadivs = District::find($village->district_id)->agadivs->lists('name', '_id');
        $postoffices = District::find($village->district_id)->postoffices->lists('name', '_id');
        if($request->input('group') == 2){
            return view('dashboard/village/edit2',['village'=>$village, 'provinces'=>$provinces, 'districts'=>$districts, 'agadivs'=>$agadivs, 'postoffices'=>$postoffices]);
        }else if($request->input('group') == 3){
            return view('dashboard/village/edit3',['village'=>$village, 'provinces'=>$provinces, 'districts'=>$districts, 'agadivs'=>$agadivs, 'postoffices'=>$postoffices]);
        }
        return view('dashboard/village/edit',['village'=>$village, 'provinces'=>$provinces, 'districts'=>$districts, 'agadivs'=>$agadivs, 'postoffices'=>$postoffices]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $village = Village::findOrFail($id); 
        $village->update($request->all()) ;
       return redirect('village/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $village = Village::findOrFail($id);
        $village->locations()->delete();
        $village->people()->delete();
        $img = $village->images()->all();
        foreach ($img as $i) {
            unlink(public_path('img/village/'.$i->name));
        }
        $village->delete(); 
        //$village = App\Models\Village::find($location->village_id);
        return redirect('village/'.$village->village_id);
    }
    public function destroyImge($vid, $id)
    {
        $village = Village::findOrFail($vid);
        $img = $village->images()->find($id);
        unlink(public_path('img/village/'.$img->name));
        $img->delete();
        //$village->images()->delete($id);
        //unlink(public_path('img/village/'.$img->name));
        return redirect('village/'.$vid);
    }
    public function upload(Request $request){

        $image = Input::file('file');
        $filename  = time() . '.' . $image->getClientOriginalExtension();

        $village_id = $request->input('village_id');        
        
        
        $village = Village::findOrFail($village_id);

        $filename  = time() . '.' . $village->_id.'.jpg';
        $path = public_path('img/village/'.$filename);

        
        Image::make($image->getRealPath())->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path);

        $image = $village->images()->create(array('name' => $filename));
        return redirect('village/'.$village_id);
    }


    public function import()
    {
        $provinces = Province::lists('name', '_id'); 
        $data = ['provinces'=>$provinces];
        return view('dashboard/village/import', $data);
    }
    public function importshow()
    {
        //return view('dashboard/village/importshow', $data);
        if (Input::file('file')->isValid()) {
            $destinationPath = 'uploads'; // upload path
            $extension = Input::file('file')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renameing image
            Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
          
            $results = Excel::selectSheets('Sheet1')->load('uploads/'.$fileName, function($reader) {
               /*
                // Getting all results
                $results = $reader->get()->toArray();
                //$data = ['results'=>$results];
                
                 foreach($results as $row)
                {
                    echo $row['english'].' - '.$row['sinhala'].'<br>';
                }*/
                
                

            })->get();
            $rows = array();
            foreach ($results as $row) {
                $input = ['name'=>$row['english'], 
                        'village_num'=>$row['number'],
                        'province_id'=> Input::get('province_id'), 
                        'district_id'=> Input::get('district_id'), 
                        'agadiv_id'=> Input::get('agadiv_id'),
                        'translate'=> ['si'=>['name'=>$row['sinhala']]]
                        ];
                        $rows[] = $input;
                Village::create($input);
            }
            unlink(public_path('uploads/'.$fileName));
            $data = ['results'=>$rows];
            return view('dashboard/village/importshow', $data);

        }
        
        
    }


}
