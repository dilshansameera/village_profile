<?php 

function translate($data, $key){
        $lang = Session::get('applocale');
        if(isset($data->translate)
        	&& isset($data->translate[$lang])
        	&& isset($data->translate[$lang][$key])){
        		return $data->translate[$lang][$key];

        }else{
        	return $data[$key];        	
        }
}

