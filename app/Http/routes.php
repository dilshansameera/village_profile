<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('public.village.show');
});

Route::get('/', 'VillageController@index');

Route::get('/village/import', 'VillageController@import');
Route::post('/village/import', 'VillageController@importshow');

Route::post('/village/upload', 'VillageController@upload');
Route::post('/location/upload/{id}', 'LocationController@upload');
Route::post('/person/upload/{id}', 'PersonController@upload');
Route::get('/village/deteleimage/{vid}/{id}', 'VillageController@destroyImge');
Route::get('/person/deteleimage/{pid}/{id}', 'PersonController@destroyImge');
Route::get('/location/deteleimage/{lid}/{id}', 'LocationController@destroyImge');

Route::get('/dashboard', function () {
    return view('dashboard.home');
});
Route::get('/provinces', 'DataController@provinces');
Route::get('/districts', 'DataController@districts');
Route::get('/agas', 'DataController@agas');
Route::get('/postal', 'DataController@postals');

Route::get('/getlist', 'VillageController@getlist');

Route::get('/language/{locale}', function ($locale) {
	Session::set('applocale', $locale);
	App::setLocale($locale);
	return Redirect::back();
});



Route::resource('/village','VillageController');
Route::resource('/location','LocationController');
Route::resource('/person','PersonController');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');

//Route::get('excel', 'ExcelController@excel');