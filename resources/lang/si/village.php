<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'name' => 'ග්‍රාම සේවා වසම (නම)',
    'locationname' => 'නම',
    'personname' => 'නම',
    'description' => 'හැදින්වීම',
    'other_names' => 'වෙනත් නම්',
    'province' => 'පළාත',
    'district' => 'දිස්ත්‍රික්කය',
    'aga_div' => 'ප්‍රාදේශීය ලේකම් කොට්ඨාශය',
    'village_num' => "ග්‍රාම සේවා වසම් අංකය",
    'postoffice' => "තැපල් කාර්යාලය",
    'nearest_town' => "ආසන්නතම නගරය",
    'climate' => "දේශගුණ තත්ව",
    'geo_variations' => "පාංශු වර්ගීකරනය",
    'geo_type' => "භූගෝලීය වර්ගීකරනය",
    'forest_type' => "වනාන්තර වර්ගීකරණය",
    'forest_desc' => "වනාන්තර පිලිබඳ විස්තරයක්",
    'plants' => "ශාඛ වර්ගීකරනය",
    'societies' => "සමිති සමාගම්",
    'groups' => "කණ්ඩායම්",
    'organisations' => "සංවිධාන",
    'business' => "ව්‍යාපාරික ස්ථාන",
    'title_location_info' => "ස්ථානීය තොරතුරු",
    'title_natural_geo_info' => "ස්වභාවික හා භුගෝල විද්‍යාත්මක තොරතුරු",
    'title_orgs_ins' => "සංවිධාන හා ආයතන",
    'title_special_loc' => "විශේෂිත ස්ථාන",
    'title_special_people' => "විශේෂිත ස්ථාන",
    'latlng' => "Location",

];
