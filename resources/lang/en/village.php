<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'name' => 'GN Divisions (Village)',
    'locationname' => 'Location Name',
    'personname' => 'Person name',
    'description' => 'Description',
    'other_names' => 'Other Names',
    'province' => 'Province',
    'district' => 'Distric',
    'aga_div' => 'AGA devision',
    'village_num' => "Village Number",
    'postoffice' => "Post office ",
    'nearest_town' => "Nearest town",
    'climate' => "Climate",
    'geo_variations' => "Geological variations",
    'geo_type' => "Geogrophy type",
    'forest_type' => "Forest type",
    'forest_desc' => "Description of Forests",
    'plants' => "Plants",
    'societies' => "Societies",
    'groups' => "Groups",
    'organisations' => "Organizations",
    'business' => "Business",
    'title_location_info' => "LOCATION INFORMATION",
    'title_natural_geo_info' => "NATURAL AND GEOGRAPHICAL INFORMATIONS",
    'title_orgs_ins' => "ORGANIZATIONS AND INSTITUES",
    'title_special_loc' => "SPECIAL LOCATIONS ",
    'title_special_people' => "SPECIAL PEOPLE",
    'latlng' => "Location",

];
