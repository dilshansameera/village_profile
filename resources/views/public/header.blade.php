<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta Http-Equiv="Cache-Control" Content="no-cache">
        <meta Http-Equiv="Pragma" Content="no-cache">
        <meta Http-Equiv="Expires" Content="0">
        <meta Http-Equiv="Pragma-directive: no-cache">
        <meta Http-Equiv="Cache-directive: no-cache">

        <title>Village profile</title>
        <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/normalize.css') }}">        
        <link rel="stylesheet" href="{{ URL::asset('css/main.css?12345') }}">
    </head>
    
    <body>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">                  
                  <ul class="nav navbar-nav navbar-right">                    
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Language<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="/language/en">EN</a></li>
                        <li><a href="/language/si">SI</a></li>            
                      </ul>
                    </li>
                    @if(Auth::check())
                     <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}}<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="/user/profile">Account</a></li> 
                        <li><a href="/auth/logout">Logout</a></li> 
                      </ul>
                    </li>
                    @else
                    <li>
                        <a href="/auth/login">Login</span></a>
                    </li>
                    @endif
                  </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="sidebar">
            <ul class="sidebar_menu" >                    
                <li><a href="/village"><span class="glyphicon glyphicon-home"></span>Villages</a></li>
                <li><a href="/location"><span class="glyphicon glyphicon-map-marker"></span>Locations</a></li>
                <li><a href="/person"><span class="glyphicon glyphicon-user"></span>People</a></li>
            </ul>
        </div>
        <div class="main_content container-fluid">
            <div class="row">