@extends('public.master')
@section('content')
<div class="col col-md-6">
    <h2>Galle</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa beatae voluptatem atque nisi cupiditate nemo autem, perferendis soluta unde nesciunt nam aspernatur quo accusantium esse! Officia saepe, exercitationem rerum iusto. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis, incidunt quidem eveniet reiciendis, asperiores minus temporibus accusantium quo fugiat harum quod, ipsa. Voluptate vero, hic eaque quidem debitis, consectetur vitae.</p>
    <h3>NATURAL AND GEOGRAPHICAL INFORMATIONS</h3>
    <p>
        <strong>climate:</strong><br>
        <strong>Geological variations:</strong><br>
        <strong>Geogrophy type:</strong><br>
        <strong>Forest type:</strong><br>
        <strong>Description of Forests:</strong><br>
        <strong>Plants :</strong><br>
    </p>
    <h3>ORGANIZATIONS AND INSTITUES</h3>
    <p>
        <strong>Societies :</strong><br>
        <strong>Groups</strong><br>
        <strong>Organizations </strong><br>
        <strong>Business:</strong><br>                       
    </p>
    <br>
    <br>
    <h2>People</h2>
    <div class="people">
        <div class="person">
            <div class="profile_pic">
                <img src="img/person/1.jpg" alt="">
            </div>
            <div class="title"><strong>Jhone Doe</strong><br>(Architect)</div>
        </div>
        <div class="person">
            <div class="profile_pic">
                <img src="{{ URL::asset('img/person/2.jpg') }}" alt="">
            </div>
            <div class="title"><strong>Jhone Doe</strong><br>(Architect)</div>
        </div>
        <div class="person">
            <div class="profile_pic">
                <img src="{{ URL::asset('img/person/1.jpg') }}" alt="">
            </div>
            <div class="title"><strong>Jhone Doe</strong><br>(Architect)</div>
        </div>
        <div class="person">
            <div class="profile_pic">
                <img src="{{ URL::asset('img/person/2.jpg') }}" alt="">
            </div>
            <div class="title"><strong>Jhone Doe</strong><br>(Architect)</div>
        </div>  
        <div class="person">
            <div class="profile_pic">
                <img src="{{ URL::asset('img/person/2.jpg') }}" alt="">
            </div>
            <div class="title"><strong>Jhone Doe</strong><br>(Architect)</div>
        </div>                       
    </div>
</div>
<div class="col col-md-6">
    <h2>Images</h2>
    <div class="image_slider">
        <div class="main_img">
            <img src="{{ URL::asset('img/village/1.jpg') }}" >                            
        </div>
        <div class="thumbs">
            <img src="{{ URL::asset('img/village/1.jpg') }}" ><img src="{{ URL::asset('img/village/2.jpg') }}" >
            <img src="{{ URL::asset('img/village/3.jpg') }}" ><img src="{{ URL::asset('img/village/4.jpg') }}" >
        </div>
    </div>
    <div class="clear"></div>    
    <br>
    <br>                
    <h2>Special Locations</h2>
    <div class="locations">
        <div class="location">
            <img src="{{ URL::asset('img/location/1.jpg') }}" alt="">
            <div><strong>Light House</strong></div>
        </div>
        <div class="location">
            <img src="{{ URL::asset('img/location/2.jpg') }}" alt="">
            <div><strong>Galle Fort</strong></div>
        </div>
        <div class="location">
            <img src="{{ URL::asset('img/location/1.jpg') }}" alt="">
            <div><strong>Galle Fort</strong></div>
        </div>
        <div class="location">
            <img src="{{ URL::asset('img/location/2.jpg') }}" alt="">
            <div><strong>Galle Fort</strong></div>
        </div>
    </div>
</div>
@endsection