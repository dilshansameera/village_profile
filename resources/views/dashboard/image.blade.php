@extends('dashboard.master')
@section('content')
{!! Form::open(['url'=>'imageupload' ,'files' => true]) !!}       
        <div class="form-group">            
            {!! Form::file('file') !!}            
        </div>        
        <button type="submit" class="btn btn-primary">Submit</button>
        {!! Form::close() !!}
@endsection