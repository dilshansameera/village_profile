@extends('public/master')
@section('content')
<div class="col-md-6"> 
	<a href="/village/{{$person->village->id}}"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> {{translate($person->village, 'name')}}</a> 
	<h1>{{translate($person, 'name')}} <a href="/person/{{ $person->id }}/edit"><span class="badge">edit</span></a></h1>   
	<p>{{translate($person, 'description')}}</p>
</div>
<div class="col-md-6"> 
	<h3>Images</h3>	
	<div class="image_wrapper">

	@foreach ($person->images as $image)  
	<div class="imgthumb">                     
	    	<a class="delimg" href="/person/deteleimage/{{{$person->id}}}/{{$image->id}}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>                       
	    <img src="{{ URL::asset('img/person/'.$image->name) }}" alt="">
	</div>
	@endforeach
	<div class="clear"></div>
	</div> 
	{!! Form::open(['url'=>'person/upload/'.$person->_id ,'files' => true]) !!}       
        <div class="form-group">   
        	<input type="hidden" class="form-control" id="name" name="person_id" value="{{ $person->id }}">         
            {!! Form::file('file') !!}            
        </div>        
        <button type="submit" class="btn btn-primary">Upload</button>
    {!! Form::close() !!}
</div>
<div class="col-md-12">
<hr>
<form action="{{ URL::route('person.destroy',$person->id) }}" method="POST">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button>Delete Person</button>
</form>
</div>
@stop
