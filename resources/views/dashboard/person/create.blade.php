@extends('public/master')

@section('content')
<div class="col-md-6">    
        {!! Form::open(['url'=>'person' ,'files' => true]) !!}
        <h3>{{ trans('village.title_special_people') }}</h3> 
        <div class="form-group">
            <input type="hidden" class="form-control" id="name" name="village_id" value="{{ $village_id }}">
            <label for="name">{{ trans('village.personname') }}</label> 
            {!! Form::text('name', null, ['class'=>'form-control']) !!}            
        </div>       
        <div class="form-group">
            <label for="description">{{ trans('village.description') }}</label>
            {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">                     
            {!! Form::file('file') !!}            
        </div>  
        <button type="submit" class="btn btn-primary">Submit</button>
        {!! Form::close() !!}    
    <br>
    <br>
    <br>
</div>
@stop
