@extends('public/master')

@section('content')
<div class="col-md-12">
	<h1>People</h1><!-- <a href="location/create"><span class="glyphicon glyphicon-plus"></span> Add new</a> -->
    <ul class="item_wrapper row">
    	 @foreach ($people as $person)                         
	    <li class="col-md-2"><a href="/person/{{ $person->_id }}">{{translate($person, 'name')}}</a></li>
		@endforeach        	
    </ul>
</div>
@stop