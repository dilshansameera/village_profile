@extends('public/master')

@section('content')
<div class="col-md-12">
	<h1>Locations</h1><!-- <a href="location/create"><span class="glyphicon glyphicon-plus"></span> Add new</a> -->
     <ul class="item_wrapper row">
    	 @foreach ($locations as $location)                         
	    <li class="col-md-2"><a href="/location/{{ $location->_id }}">{{translate($location, 'name')}}</a></li>
		@endforeach        	
    </ul>
</div>
@stop