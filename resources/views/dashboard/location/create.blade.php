@extends('public/master')

@section('content')
<div class="col-md-6">    
        {!! Form::open(['url'=>'location' ,'files' => true]) !!}
        <h3>{{ trans('village.title_special_loc') }}</h3> 
        <div class="form-group">
            <input type="hidden" class="form-control" id="name" name="village_id" value="{{ $village_id }}">
            <label for="name">{{ trans('village.locationname') }}</label> 
            {!! Form::text('name', null, ['class'=>'form-control']) !!}            
        </div>       
        <div class="form-group">
            <label for="description">{{ trans('village.description') }}</label>
            {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">                     
            {!! Form::file('file') !!}            
        </div> 
        <div class="form-group">
            <label for="name">{{ trans('village.latlng') }}</label>
            <input type="text" class="form-control latlng" id="latlng" name="latlng" readonly="readonly">            
        </div> 
        <button type="submit" class="btn btn-primary">Submit</button>
        {!! Form::close() !!}    
    <br>
    <br>
    <br>
</div>
@stop


@section('script')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;signed_in=true"></script>
<script>
$(document).ready(function(){
    $('.latlng').on('click', function(e){
        $('.lightbox').show();
        $('.light-content').show();
        var province = "{{$village->province->name}}";        
        var district = "{{$village->district->name}}";
        var agadiv = "{{$village->agadiv->name}}";
        
        if (province === 'Please select') {
             province = '';
        };
        if (district === 'Please select') {
             district = '';
        };
        if (agadiv === 'Please select') {
             agadiv = '';
        };
        var txt ='';
       
        
        if (agadiv != '') {
            txt += agadiv+', '
        };
        if (district != '' ) {
            txt += district+' District, '
        };
         if (province != '') {
            txt += province+' Province, '
        };
        txt += 'Sri Lanka'
        console.log(txt );
        initMap(txt);
    });   
    $('.lightbox').on('click', function(e){
        $('.lightbox').hide();
        $('.light-content').hide();
        
    });


    function initMap(txt) {
      // Create a map object and specify the DOM element for display.
        var map;
        var markers = [];
        var latlng = $('#latlng').val();
        if (latlng !='') {
                latlng = latlng.split(',');
        }
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
        'address': txt
        }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
           
            if ($('#latlng').val() !='') {                
                var myOptions = {
                zoom: 10,
                center: {lat: parseFloat(latlng[0]), lng: parseFloat(latlng[1])},
                mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            }else{
                var myOptions = {
                    zoom: 10,
                    center: results[0].geometry.location,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            }
                
            map = new google.maps.Map(document.getElementById("map"), myOptions);
            
            var marker = new google.maps.Marker({
                position: {lat: parseFloat(latlng[0]), lng: parseFloat(latlng[1])}, 
                map: map
            });
            markers.push(marker);

            google.maps.event.addListener(map, "click", function(e) {  

                placeMarker(e.latLng);
                $('#latlng').val(e.latLng.lat()+','+e.latLng.lng());
                //console.log("Latitude: " + e.latLng.lat() + " Longitude: " + e.latLng.lng());
            });
            /*var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });*/
        }
        });
        
        function placeMarker(location) {
            clearMarkers();
            var marker = new google.maps.Marker({
                position: location, 
                map: map
            });
            markers.push(marker);
        }
        function clearMarkers() {
          for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
          }
        }

    }
});
</script>  
  
@stop
