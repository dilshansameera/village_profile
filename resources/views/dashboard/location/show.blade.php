@extends('public/master')
@section('content')
<div class="col-md-6">
	<a href="/village/{{$location->village->id}}"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"> </span> {{translate($location->village, 'name')}}</a> 
	<h1>{{translate($location, 'name')}} <a href="/location/{{ $location->id }}/edit"><span class="badge">edit</span></a></h1>   
	<p>{{translate($location, 'description')}}</p>
</div>
<div class="col-md-6">
	<div class="row">
		<div class="col-md-12 mapouter"> 
			<div class="map mapbox" id="mapbox" ></div>			
		</div>

		<div class="col-md-12"> 
			<h3>Images</h3>	
			<div class="image_wrapper">

			@foreach ($location->images as $image)    
			<div class="imgthumb">                     
			    <a class="delimg" href="/location/deteleimage/{{{$location->id}}}/{{$image->id}}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>                    
			    <img src="{{ URL::asset('img/location/'.$image->name) }}" alt="">
			</div>
			@endforeach
				<div class="clear"></div>
			</div> 
			{!! Form::open(['url'=>'location/upload/'.$location->_id ,'files' => true]) !!}       
		        <div class="form-group">   
		        	<input type="hidden" class="form-control" id="name" name="location_id" value="{{ $location->id }}">         
		            {!! Form::file('file') !!}            
		        </div>        
		        <button type="submit" class="btn btn-primary">Upload</button>
		    {!! Form::close() !!}
		</div>
	</div>
</div>
		
<div class="col-md-12">
<hr>
<form action="{{ URL::route('location.destroy',$location->id) }}" method="POST">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button>Delete Location</button>
</form>
</div>
@stop

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;signed_in=true"></script>
<script>
$(document).ready(function(){
    
	initMap();

    function initMap() {
      // Create a map object and specify the DOM element for display.
        var map;
        
        var latlng ="{{ $location->latlng }}";
        if (latlng !='') {
                latlng = latlng.split(',');
        }
             
        var myOptions = {
        	zoom: 14,
	        center: {lat: parseFloat(latlng[0]), lng: parseFloat(latlng[1])},
	        mapTypeId: google.maps.MapTypeId.ROADMAP
	        }
                
        	map = new google.maps.Map(document.getElementById("mapbox"), myOptions);
        
	        var marker = new google.maps.Marker({
	            position: {lat: parseFloat(latlng[0]), lng: parseFloat(latlng[1])}, 
	            map: map
	        });
            
        }
        });
        

</script>  
  
@stop