@extends('public/master')

@section('content')
<div class="col-md-12">
	<h1>Villages</h1>
	<a href="village/create"><span class="glyphicon glyphicon-plus"></span> Add new</a>   
	<a href="village/import"><span class="glyphicon glyphicon-plus"></span> Import</a> 
	<div class="panel panel-default">
		<div class="panel-heading">Search</div>
		<div class="panel-body">

			{!! Form::open(['url'=>'village', 'method'=>'GET']) !!}
			  <div class="form-group">
			  	<label for="province">{{ trans('village.province') }}</label>
            	{!! Form::select('province_id', [null=>'Please select'] + $provinces->toArray(), null, ['class'=>'form-control', 'id'=>'province']) !!}			   
			  </div>
			  <div class="form-group">
			    <label for="district">{{ trans('village.district') }}</label>
            	{!! Form::select('district_id', array(), null, ['class'=>'form-control', 'id'=>'district']) !!}
			  </div>
			  <div class="form-group">
			    <label for="aga_div">{{ trans('village.aga_div') }}</label>
            	{!! Form::select('agadiv_id', array(), null, ['class'=>'form-control', 'id'=>'agadiv']) !!}
			  </div>
			  <button type="submit" class="btn btn-default">Search</button>			
        	{!! Form::close() !!}  

		</div>
	</div>
	
    <ul class="item_wrapper row">
    	 @foreach ($villages as $village)                         
	    <li class="col-md-2"><a href="/village/{{ $village->id }}">{{translate($village, 'name')}}</a></li>
		@endforeach        	
    </ul>
</div>
@stop

@section('script')

