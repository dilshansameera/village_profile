@extends('public/master')

@section('content')
<div class="col-md-6">    
        {!! Form::model($village, ['method'=>'PATCH', 'url'=>'village/'.$village->_id]) !!}
        <h3>{{ trans('village.title_natural_geo_info') }}</h3>        
        <div class="form-group">
            <label for="name">{{ trans('village.climate') }}</label>
            {!! Form::text('climate', null, ['class'=>'form-control', 'id'=>'climate']) !!}            
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.geo_variations') }}</label>
            {!! Form::text('geo_variations', null, ['class'=>'form-control', 'id'=>'geo_variations']) !!}            
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.geo_type') }}</label>
            {!! Form::text('geo_type', null, ['class'=>'form-control', 'id'=>'geo_type']) !!}            
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.forest_type') }}</label>
            {!! Form::text('forest_type', null, ['class'=>'form-control', 'id'=>'forest_type']) !!}            
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.forest_desc') }}</label>
            {!! Form::text('forest_desc', null, ['class'=>'form-control', 'id'=>'forest_desc']) !!}            
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.plants') }}</label>
            {!! Form::text('plants', null, ['class'=>'form-control', 'id'=>'plants']) !!}            
        </div>
        <button type="submit" class="btn btn-primary">Save</button> <a href="/village/{{ $village->id }}">Cancel</a>
        {!! Form::close() !!}    

</div>
@stop
