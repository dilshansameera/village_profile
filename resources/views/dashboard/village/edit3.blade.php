@extends('public/master')

@section('content')
<div class="col-md-6">    
        {!! Form::model($village, ['method'=>'PATCH', 'url'=>'village/'.$village->_id]) !!}
        <h3>{{ trans('village.title_orgs_ins') }}</h3>          
        <div class="form-group">
            <label for="name">{{ trans('village.societies') }}</label>
            {!! Form::text('societies', null, ['class'=>'form-control', 'id'=>'societies']) !!}            
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.groups') }}</label>
            {!! Form::text('groups', null, ['class'=>'form-control', 'id'=>'groups']) !!}            
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.organisations') }}</label>
            {!! Form::text('organisations', null, ['class'=>'form-control', 'id'=>'organisations']) !!}            
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.business') }}</label>
            {!! Form::text('business', null, ['class'=>'form-control', 'id'=>'business']) !!}            
        </div>        
        <button type="submit" class="btn btn-primary">Save</button> <a href="/village/{{ $village->id }}">Cancel</a>
        {!! Form::close() !!}    

</div>
@stop
