@extends('public/master')

@section('content')
<div class="col-md-6"> 
    
        {!! Form::open(['url'=>'village']) !!}
        <h3>{{ trans('village.title_location_info') }}</h3>        
        <div class="form-group">
            <label for="province">{{ trans('village.province') }}</label>
            {!! Form::select('province', [null=>'Please Select'] + $provinces->toArray(), null, ['class'=>'form-control', 'id'=>'province']) !!}            
        </div>
        <div class="form-group">
            <label for="district">{{ trans('village.district') }}</label>
            {!! Form::select('province', array(), null, ['class'=>'form-control', 'id'=>'district']) !!}
            
        </div>
        <div class="form-group">
            <label for="aga_div">{{ trans('village.aga_div') }}</label>
            {!! Form::select('agadiv', array(), null, ['class'=>'form-control', 'id'=>'agadiv']) !!}
        </div>
        <div class="form-group">
            <label for="village_num">{{ trans('village.name') }}</label>
            {!! Form::select('gndiv', array(), null, ['class'=>'form-control', 'id'=>'gndiv']) !!}
        </div>
        <div class="form-group">
            <label for="other_names">{{ trans('village.other_names') }}</label>
            <input type="text" class="form-control" id="name" name="other_names">
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.postoffice') }}</label>
            {!! Form::select('postoffice', array(), null, ['class'=>'form-control', 'id'=>'postoffice']) !!}
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.nearest_town') }}</label>
            <input type="text" class="form-control" id="name" name="nearest_town">
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.description') }}</label>
           <textarea type="text" class="form-control" id="name" name="description"/></textarea>
            
        </div>
        <hr>
        <h3>{{ trans('village.title_natural_geo_info') }}</h3>        
        <div class="form-group">
            <label for="name">{{ trans('village.climate') }}</label>
            <input type="text" class="form-control" id="name" name="climate">
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.geo_variations') }}</label>
            <input type="text" class="form-control" id="name" name="geo_variations">
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.geo_type') }}</label>
            <input type="text" class="form-control" id="name" name="geo_type">
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.forest_type') }}</label>
            <input type="text" class="form-control" id="name" name="forest_type" >
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.forest_desc') }}</label>
            <input type="text" class="form-control" id="name" name="forest_desc">
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.plants') }}</label>
            <input type="text" class="form-control" id="name" name="plants">
        </div>
        <hr>
        <h3>{{ trans('village.title_orgs_ins') }}</h3>          
        <div class="form-group">
            <label for="name">{{ trans('village.societies') }}</label>
            <input type="text" class="form-control" id="name" name="societies">
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.groups') }}</label>
            <input type="text" class="form-control" id="name" name="groups">
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.organisations') }}</label>
            <input type="text" class="form-control" id="name" name="organisations">
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.business') }}</label>
            <input type="text" class="form-control" id="name" name="business">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        {!! Form::close() !!}
    
    <br>
    <br>
    <br>
</div>

@stop
