@extends('public/master')

@section('content')
<div class="col-md-6">    
        {!! Form::model($village, ['method'=>'PATCH', 'url'=>'village/'.$village->_id]) !!}
        <h3>{{ trans('village.title_location_info') }}</h3>        
        <div class="form-group">
            <label for="province">{{ trans('village.province') }}</label>
            {!! Form::select('province_id', [null=>'Please Select'] + $provinces->toArray(), $village->province_id, ['class'=>'form-control', 'id'=>'province']) !!}            
        </div>
        <div class="form-group">
            <label for="district">{{ trans('village.district') }}</label>
            {!! Form::select('district_id', $districts, $village->district_id, ['class'=>'form-control', 'id'=>'district']) !!}
            
        </div>
        <div class="form-group">
            <label for="aga_div">{{ trans('village.aga_div') }}</label>
            {!! Form::select('agadiv_id', $agadivs, $village->agadiv_id, ['class'=>'form-control', 'id'=>'agadiv']) !!}
        </div>
        <div class="form-group">
            <label for="village_num">{{ trans('village.name') }}</label> 
            {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'gndiv']) !!}                       
            
        </div>
        <div class="form-group">
            <label for="other_names">{{ trans('village.other_names') }}</label>
            {!! Form::text('othernames', null, ['class'=>'form-control', 'id'=>'othernames']) !!}            
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.postoffice') }}</label>
            {!! Form::select('postoffice', $postoffices, $village->postoffice, ['class'=>'form-control', 'id'=>'postoffice']) !!}
            <!-- {!! Form::text('postoffice', null, ['class'=>'form-control', 'id'=>'postoffice']) !!} -->
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.nearest_town') }}</label>
            {!! Form::text('nearest_town', null, ['class'=>'form-control', 'id'=>'nearest_town']) !!}
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.description') }}</label>
            {!! Form::textarea('description', null, ['class'=>'form-control', 'id'=>'agadiv']) !!}              
        </div>
        <div class="form-group">
            <label for="name">{{ trans('village.latlng') }}</label>
            {!! Form::text('latlng', null, ['readonly'=>'readonly' ,'class'=>'form-control latlng', 'id'=>'latlng']) !!} 
            
        </div>
       
        <button type="submit" class="btn btn-primary">Save</button> <a href="/village/{{ $village->id }}">Cancel</a>
        {!! Form::close() !!}    

</div>
@stop

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;signed_in=true"></script>
<script>
$(document).ready(function(){
    $('.latlng').on('click', function(e){
        $('.lightbox').show();
        $('.light-content').show();
        var province = $("#province option:selected").text();        
        var district = $("#district option:selected").text();
        var agadiv = $("#agadiv option:selected").text();;
        
        if (province === 'Please select') {
             province = '';
        };
        if (district === 'Please select') {
             district = '';
        };
        if (agadiv === 'Please select') {
             agadiv = '';
        };
        var txt ='';
       
        
        if (agadiv != '') {
            txt += agadiv+', '
        };
        if (district != '' ) {
            txt += district+' District, '
        };
         if (province != '') {
            txt += province+' Province, '
        };
        txt += 'Sri Lanka'
        console.log(txt );
        initMap(txt);
    });   
    $('.lightbox').on('click', function(e){
        $('.lightbox').hide();
        $('.light-content').hide();
        
    });


    function initMap(txt) {
      // Create a map object and specify the DOM element for display.
        var map;
        var markers = [];
        var latlng = $('#latlng').val();
        if (latlng !='') {
                latlng = latlng.split(',');
        }
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
        'address': txt
        }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
           
            if ($('#latlng').val() !='') {                
                var myOptions = {
                zoom: 10,
                center: {lat: parseFloat(latlng[0]), lng: parseFloat(latlng[1])},
                mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            }else{
                var myOptions = {
                    zoom: 10,
                    center: results[0].geometry.location,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            }
                
            map = new google.maps.Map(document.getElementById("map"), myOptions);
            
            var marker = new google.maps.Marker({
                position: {lat: parseFloat(latlng[0]), lng: parseFloat(latlng[1])}, 
                map: map
            });
            markers.push(marker);

            google.maps.event.addListener(map, "click", function(e) {  

                placeMarker(e.latLng);
                $('#latlng').val(e.latLng.lat()+','+e.latLng.lng());
                //console.log("Latitude: " + e.latLng.lat() + " Longitude: " + e.latLng.lng());
            });
            /*var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });*/
        }
        });
        
        function placeMarker(location) {
            clearMarkers();
            var marker = new google.maps.Marker({
                position: location, 
                map: map
            });
            markers.push(marker);
        }
        function clearMarkers() {
          for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
          }
        }

    }
});
</script>  
  
@stop