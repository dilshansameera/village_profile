@extends('public/master')
@section('content')
<div class="col-md-6"> 
	<h1>{{translate($village, 'name')}} <a href="/village/{{ $village->id }}/edit"><span class="badge">edit</span></a></h1>   
	<p>{{translate($village, 'description')}}</p>
	<h3>{{ trans('village.title_location_info') }}</h3>	
	<p>{{translate($village->province, 'name')}} province, {{translate($village->district, 'name')}} District, {{translate($village->agadiv, 'name')}} AGA Division</p>		
	<p><strong>{{ trans('village.postoffice') }} : </strong>{{translate($village->postal, 'name')}} - {{translate($village->postal, 'postal_code')}}</p>			        
	<p><strong>{{ trans('village.nearest_town') }} : </strong>{{translate($village, 'nearest_town')}}</p>	
	<hr>
	<h3>{{ trans('village.title_natural_geo_info') }} <a href="/village/{{ $village->id }}/edit?group=2"><span class="badge">edit</span></a></h3>			
	<p><strong>{{ trans('village.climate') }}: </strong>{{translate($village, 'climate')}}</p>
	<p><strong>{{ trans('village.geo_variations') }} : </strong>{{translate($village, 'geo_variations')}}</p>			        
	<p><strong>{{ trans('village.geo_type') }} : </strong>{{translate($village, 'geo_type')}}</p>			        
	<p><strong>{{ trans('village.forest_type') }} : </strong>{{translate($village, 'forest_type')}}</p>			        
	<p><strong>{{ trans('village.forest_desc') }} : </strong>{{translate($village, 'forest_desc')}}</p>			        
	<p><strong>{{ trans('village.plants') }} : </strong>{{translate($village, 'plants')}}</p>
	<hr>
	<h3>{{ trans('village.title_orgs_ins') }}  <a href="/village/{{ $village->id }}/edit?group=3"><span class="badge">edit</span></a></h3>			
	<p><strong>{{ trans('village.societies') }} : </strong>{{translate($village, 'societies')}}</p>
	<p><strong>{{ trans('village.groups') }} : </strong>{{translate($village, 'groups')}}</p>			        
	<p><strong>{{ trans('village.organisations') }} : </strong>{{translate($village, 'organisations')}}</p>			        
	<p><strong>{{ trans('village.business') }}: </strong>{{translate($village, 'business')}}</p>			        
	

</div>
<div class="col-md-6">
	<div class="row">
		<div class="col-md-12 mapouter"> 
			<div class="map mapbox" id="mapbox" ></div>			
		</div>
		<div class="col-md-12"> 
			<h3>Images</h3>	
			<div class="image_wrapper">

			@foreach ($village->images as $image)
				<div class="imgthumb">                     
			    	<a class="delimg" href="/village/deteleimage/{{{$village->id}}}/{{$image->id}}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
			    	<img src="{{ URL::asset('img/village/'.$image->name) }}" alt="">
			    </div>    
			@endforeach
				<div class="clear"></div>
			</div> 
			{!! Form::open(['url'=>'village/upload' ,'files' => true]) !!}       
		        <div class="form-group">   
		        	<input type="hidden" class="form-control" id="name" name="village_id" value="{{ $village->id }}">         
		            {!! Form::file('file') !!}            
		        </div>        
		        <button type="submit" class="btn btn-primary">Upload</button>
		    {!! Form::close() !!}
		</div>
	</div>
</div>

<div class="col-md-12">
<hr>
	<h3>{{ trans('village.title_special_loc') }}</h3>
	<a href="/location/create?village_id={{$village->id}}"><span class="glyphicon glyphicon-plus"></span> Add  Special location</a><br>
	<div class="image_wrapper row">
		@foreach ($locations as $image)
			<div class="col-md-6  imgthumb">				
				<a href="/location/{{$image->id}}">
					<div>
						@if (isset($image->images[0])) 	   
						<img src="{{ URL::asset('img/location/'.$image->images[0]->name) }}" alt="">
						@endif
					</div>
					<span>{{$image->name}}</span>
				</a> 
			</div>			
		@endforeach
		<div class="clear"></div>
	</div>


	<hr>
	<h3>{{ trans('village.title_special_people') }}</h3>
	<a href="/person/create?village_id={{$village->id}}"><span class="glyphicon glyphicon-plus"></span> Add  a person</a><br>
	<div class="image_wrapper row">
		@foreach ($people as $image)
			<div class="col-md-6 imgthumb">				
				<a href="/person/{{$image->id}}">
					<div>
						@if (isset($image->images[0])) 	   
						<img src="{{ URL::asset('img/person/'.$image->images[0]->name) }}" alt="">
						@endif
					</div>
					<span>{{$image->name}}</span>
				</a> 
			</div>			
		@endforeach
		<div class="clear"></div>
	</div>
<hr>
<form action="{{ URL::route('village.destroy',$village->id) }}" method="POST">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button>Delete Village</button>
</form>	
	
</div>

@stop

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;signed_in=true"></script>
<script>
$(document).ready(function(){
    
	initMap();

    function initMap() {
      // Create a map object and specify the DOM element for display.
        var map;
        
        var latlng ="{{ $village->latlng }}";
        if (latlng !='') {
                latlng = latlng.split(',');
        }
             
        var myOptions = {
        	zoom: 14,
	        center: {lat: parseFloat(latlng[0]), lng: parseFloat(latlng[1])},
	        mapTypeId: google.maps.MapTypeId.ROADMAP
	        }
                
        	map = new google.maps.Map(document.getElementById("mapbox"), myOptions);
        
	        var marker = new google.maps.Marker({
	            position: {lat: parseFloat(latlng[0]), lng: parseFloat(latlng[1])}, 
	            map: map
	        });
            
        }
        });
        

</script>  
  
@stop
