@extends('public/master')

@section('content')
<div class="col-md-6"> 
    
        {!! Form::open(['url'=>'village/import','files' => true]) !!}
        <h3>{{ trans('village.title_location_info') }}</h3>        
        <div class="form-group">
            <label for="province">{{ trans('village.province') }}</label>
            {!! Form::select('province_id', [null=>'Please select'] + $provinces->toArray(), null, ['class'=>'form-control', 'id'=>'province']) !!}            
        </div>
        <div class="form-group">
            <label for="district">{{ trans('village.district') }}</label>
            {!! Form::select('district_id', array(), null, ['class'=>'form-control', 'id'=>'district']) !!}
            
        </div>
        <div class="form-group">
            <label for="aga_div">{{ trans('village.aga_div') }}</label>
            {!! Form::select('agadiv_id', array(), null, ['class'=>'form-control', 'id'=>'agadiv']) !!}
        </div>        
        <div class="form-group">   
            {!! Form::file('file') !!}            
        </div>        
        <hr>        
        <button type="submit" class="btn btn-primary">Submit</button>
        {!! Form::close() !!}    
    <br>
    <br>
    <br>
</div>
@stop


