@extends('auth.master')
@section('content')
<!-- resources/views/auth/login.blade.php -->

    <form method="POST" action="/auth/login">
    {!! csrf_field() !!}

    <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
        <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email"  value="{{ old('email') }}">

    </div>

   <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
        <input type="password" name="password" id="password"  class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="remember"> Remember Me
        </label>
    </div>

    <div>
        <button type="submit" class="btn btn-default">Submit</button> 
        <a href="/auth/register">Register</a>
        <a href="/auth/facebook">Login with FB</a>

    </div>

</form>
@endsection