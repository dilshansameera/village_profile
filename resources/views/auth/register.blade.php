@extends('auth.master')
@section('content')
<!-- resources/views/auth/register.blade.php -->

<form method="POST" action="/auth/register">
    {!! csrf_field() !!}

    <div class="form-group">
        <label for="exampleInputEmail1">Name</label>
        <input  class="form-control"  type="text" name="name" value="{{ old('name') }}">
    </div>

    <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input  class="form-control"  type="email" name="email" value="{{ old('email') }}">
    </div>

    <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
        <input  class="form-control"  type="password" name="password">
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1">Confirm Password</label>
        <input class="form-control"  type="password" name="password_confirmation">
    </div>

    <div>
        <button type="submit" class="btn btn-default">Register</button> 
        <a href="/auth/login">Login</a>
    </div>
</form>
@endsection